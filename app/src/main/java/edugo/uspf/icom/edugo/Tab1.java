package edugo.uspf.icom.edugo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by danieltunde on 19-September-2015.
 */
public class Tab1 extends Fragment {

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_1, container, false);


//        // Calling the RecyclerView
//        mRecyclerView = (RecyclerView)rootView.findViewById(R.id.recycler_view);
//        mRecyclerView.setHasFixedSize(true);

       mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
//
//        // The number of Columns
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new GridAdapter();
        mRecyclerView.setAdapter(mAdapter);

        return v;
//
//        mAdapter = new GridAdapter();
//        mRecyclerView.setAdapter(mAdapter);
    }



}