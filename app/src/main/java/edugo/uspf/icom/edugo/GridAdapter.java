package edugo.uspf.icom.edugo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danieltunde on 19-September-2015.
 */
public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {

    List<FilesItem> mItems;

    public GridAdapter() {
        super();
        mItems = new ArrayList<FilesItem>();
        FilesItem file = new FilesItem();
        file.setName("File 1");
        file.setThumbnail(R.mipmap.file);
        mItems.add(file);

        file = new FilesItem();
        file.setName("File 2");
        file.setThumbnail(R.mipmap.file);
        mItems.add(file);

        file = new FilesItem();
        file.setName("File 3");
        file.setThumbnail(R.mipmap.file);
        mItems.add(file);

        file = new FilesItem();
        file.setName("File 4");
        file.setThumbnail(R.mipmap.file);
        mItems.add(file);

        file = new FilesItem();
        file.setName("File 5");
        file.setThumbnail(R.mipmap.file);
        mItems.add(file);

        file = new FilesItem();
        file.setName("File 6");
        file.setThumbnail(R.mipmap.file);
        mItems.add(file);

        file = new FilesItem();
        file.setName("File 7");
        file.setThumbnail(R.mipmap.file);
        mItems.add(file);

        file = new FilesItem();
        file.setName("File 8");
        file.setThumbnail(R.mipmap.file);
        mItems.add(file);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.grid_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        FilesItem nature = mItems.get(i);
        viewHolder.fileName.setText(nature.getName());
        viewHolder.imgThumbnail.setImageResource(nature.getThumbnail());
    }

    @Override
    public int getItemCount() {

        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imgThumbnail;
        public TextView fileName;

        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbnail = (ImageView)itemView.findViewById(R.id.img_thumbnail);
            fileName = (TextView)itemView.findViewById(R.id.file_name);
        }
    }
}
