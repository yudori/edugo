package edugo.uspf.icom.edugo;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by danieltunde on 19-September-2015.
 */
public class WebLink {

    public static String sendRequest(String sURL, String requestType, String data){
        HttpURLConnection connection = null;
        StringBuffer response = new StringBuffer();
        try {
            URL url = new URL(sURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(requestType);
            connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length","" + Integer.toString(data.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");
            //connection.setUseCaches(false);
            //connection.setDoInput(true);
            //connection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            String responseStr = response.toString();
            Log.d("Server response", responseStr);
        } catch (Exception e) {

            e.printStackTrace();

        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return response.toString();
    }

    public List<DocFile> readFileList(String json){
        List<DocFile> files = new ArrayList<DocFile>();
        JSONArray jArray = null;
        try {
            jArray = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jArray != null) {
            try {
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject entry = jArray.getJSONObject(i);
                    String name = entry.getString("name");
                    String path = entry.getString("path");
                    files.add(new DocFile(name, path));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return files;
    }

    public class DocFile{
        //private UUID id;
        private String name;
        private String path;

        public DocFile(String name, String path){
            this.name = name;
            this.path = path;
        }
    }
}
